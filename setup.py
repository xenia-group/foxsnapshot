#!/usr/bin/env python3

import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "foxsnapshot",
    version = "0.0.1",
    author = "Luna Deards",
    author_email = "luna@xenialinux.com",
    description= ("btrfs snapshot manager made for Xenia Linux"),
    license = "GPL-3",
    keywords = "snapshot btrfs",
    url = "https://gitlab.com/xenia-group/foxsnapshot",
    packages = ['foxsnapshot'],
    entry_points={
        'console_scripts': [
            'foxsnapshot = foxsnapshot.foxsnapshot:main',
        ],
    },
    requires=["subprocess", "sys", "argparse", "os", "time", "foxcommon"],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Automation"
        "License :: OSI Approved :: GPL-3"
    ],
)
