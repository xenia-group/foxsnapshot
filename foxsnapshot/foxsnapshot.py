import sys, argparse, os, time, foxcommon

PRETEND = False


def btrfs() -> bool:
    if len(foxcommon.execute("df -t btrfs /roots | grep -v Filesystem", override=True)) > 0:
        return True
    
    return False


def get_snapshots() -> list:
    snapshots = {}

    for snapshot in os.listdir("/roots/.foxsnapshot"):
        # This gets the creation time of the snapshot, formats it properly, then converts it to a format Python understands.
        # I hate this more than you do.
        creation = time.strptime(
            foxcommon.execute(f'btrfs subvol show /roots/.foxsnapshot/{snapshot}/ | grep "Creation time:"', override=True).decode().split("Creation time:")[1].strip(),
            "%Y-%m-%d %H:%M:%S %z"
        )
        snapshots[snapshot] = creation

    snapshots = sorted(snapshots.items(), key=lambda x:x[1])
    return(snapshots)


def revert_check() -> bool:
    if os.path.exists("/roots/.revert"):
        foxcommon.warn("A revert operation is due to take place. Either cancel this revert with foxsnapshot revert --cancel or reboot to complete the revert operation.")
        return False

    return True


def create_snapshot(name) -> None:
    if revert_check():
        foxcommon.execute(f"btrfs subvolume snapshot /overlay/usr /roots/.foxsnapshot/{name}")


def delete_snapshot(name) -> None:
    if revert_check():
        if name not in [k for k, _ in get_snapshots()]:
            foxcommon.die(f"Snapshot '{name}' does not exist.")

        foxcommon.execute(f"btrfs subvolume delete /roots/.foxsnapshot/{name}")


def revert_snapshot(name=None, cancel=False) -> None:
    if cancel == True:
        current = open("/roots/.revert")
        foxcommon.info(f"Cancelling revert operation to snapshot {current.read()}")
        current.close()
        os.remove("/roots/.revert")
        exit()

    if revert_check():
        snapshots = get_snapshots()

        if name == None and len(snapshots) > 1: # More than just initial snapshot
            name = snapshots[-2][0]
        elif name == None and len(snapshots) == 1: # Initial snapshot only, 
            name = snapshots[-1][0]
        
        if name not in [snapshot[0] for snapshot in snapshots]:
            foxcommon.die(f"Snapshot name '{name}' does not exist, run foxsnapshot list to see available snapshots.")

        foxcommon.info(f"Snapshot specified: {name}")
        foxcommon.info("Setting snapshot revert flag. Please reboot for changes to take effect, or run foxsnapshot revert --cancel.")

        with open("/roots/.revert", "w") as revert:
            revert.write(name)


def parse_args():
    parser = argparse.ArgumentParser(prog="foxsnapshot", description="Manages snapshots of /usr in a Xenia system.")
    subparser = parser.add_subparsers(help="Action to take.", required=True, dest="action")

    create_parser = subparser.add_parser("create")
    delete_parser = subparser.add_parser("delete")
    revert_parser = subparser.add_parser("revert")
    subparser.add_parser("list")

    create_parser.add_argument("name", help="Name of snapshot to take")
    delete_parser.add_argument("name", help="Name of snapshot to delete")
    revert_parser.add_argument("-c", "--cancel", help="Cancel the current revert operation.", action="store_true")
    revert_parser.add_argument("-n", "--name", help="Name of snapshot to revert to. Defaults to last snapshot if not set.", required=False)

    args = parser.parse_args()
    return vars(args)


def snapshot_checks() -> None:
    if not os.path.exists("/roots/.foxsnapshot"):
        foxcommon.info("Snapshots subvolume does not exist. Creating now.")
        foxcommon.execute("btrfs subvolume create /roots/.foxsnapshot")
    
    if not os.path.exists("/roots/.foxsnapshot/initial"):
        foxcommon.info("Initial snapshot does not exist. Creating now.")
        foxcommon.execute("btrfs subvolume snapshot /overlay/usr /roots/.foxsnapshot/initial")


def main():
    if os.geteuid() != 0:
        foxcommon.die("foxsnapshot requires root privileges to run.")
    
    if not btrfs():
        foxcommon.die("foxsnapshot is only available on btrfs systems.")

    action = parse_args()

    snapshot_checks()

    match action["action"]:
        case "create":
            create_snapshot(action["name"])
        case "delete":
            delete_snapshot(action["name"])
        case "revert":
            revert_snapshot(action["name"], action["cancel"])
        case "list":
            print(*[f"{time.strftime('%Y-%m-%d', v)}: {k}" for k,v in get_snapshots()], sep="\n")


if __name__ == "__main__":
    main()
